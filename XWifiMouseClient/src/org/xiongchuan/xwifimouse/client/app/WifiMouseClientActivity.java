package org.xiongchuan.xwifimouse.client.app;

import java.io.OutputStream;
import java.net.Socket;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

public class WifiMouseClientActivity extends Activity implements OnClickListener {
    /** Called when the activity is first created. */
	
	private Socket socket;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        findViewById(R.id.connectBtn).setOnClickListener(this);
        findViewById(R.id.handle_close).setOnClickListener(this);
        findViewById(R.id.handle_down).setOnClickListener(this);
        findViewById(R.id.handle_enter).setOnClickListener(this);
        findViewById(R.id.handle_left).setOnClickListener(this);
        findViewById(R.id.handle_right).setOnClickListener(this);
        findViewById(R.id.handle_up).setOnClickListener(this);
        findViewById(R.id.handle_esc).setOnClickListener(this);
    }

	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.connectBtn:
			try {
				socket = connect();
				findViewById(R.id.connectView).setVisibility(View.GONE);
				findViewById(R.id.clickView).setVisibility(View.VISIBLE);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				alert("连接失败，请检查服务器ip地址和端口是否填写正确！");
				e.printStackTrace();
			}
				break;
		case R.id.handle_close:
			try {
				send("[close]");
				disconnect();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			findViewById(R.id.connectView).setVisibility(View.VISIBLE);
			findViewById(R.id.clickView).setVisibility(View.GONE);
			break;
		case R.id.handle_left:
			try {
				send("[left]");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				findViewById(R.id.connectView).setVisibility(View.VISIBLE);
				findViewById(R.id.clickView).setVisibility(View.GONE);
			}
			break;
		case R.id.handle_enter:
			try {
				send("[enter]");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				findViewById(R.id.connectView).setVisibility(View.VISIBLE);
				findViewById(R.id.clickView).setVisibility(View.GONE);
			}
			break;
		case R.id.handle_right:
			try {
				send("[right]");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.i("Mouse",e.getMessage());
				findViewById(R.id.connectView).setVisibility(View.VISIBLE);
				findViewById(R.id.clickView).setVisibility(View.GONE);
				
			}
			break;
		case R.id.handle_down:
			try {
				send("[down]");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.i("Mouse",e.getMessage());
				findViewById(R.id.connectView).setVisibility(View.VISIBLE);
				findViewById(R.id.clickView).setVisibility(View.GONE);
				
			}
			break;
		case R.id.handle_up:
			try {
				send("[up]");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.i("Mouse",e.getMessage());
				findViewById(R.id.connectView).setVisibility(View.VISIBLE);
				findViewById(R.id.clickView).setVisibility(View.GONE);
				
			}
			break;
		case R.id.handle_esc:
			try {
				send("[esc]");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.i("Mouse",e.getMessage());
				findViewById(R.id.connectView).setVisibility(View.VISIBLE);
				findViewById(R.id.clickView).setVisibility(View.GONE);
				
			}
			break;
		}
	}
	
	/**
	 * 返回套接字句�?	 * @return Socket
	 * @throws Exception
	 */
	public Socket connect() throws Exception{
		EditText etIp = (EditText) findViewById(R.id.ip);
		String host = etIp.getText().toString();
		EditText etPort = (EditText) findViewById(R.id.port);
		int port = Integer.parseInt(etPort.getText().toString());
		return new Socket(host,port);
	}
	
	/**
	 * 断开套接连接
	 * @throws Exception
	 */
	public void disconnect() throws Exception{
		if(null != socket){
			socket.close();
		}
	}
	
	public void send(String str) throws Exception{
		if(true==socket.isConnected()){
			Log.i("Mouse",str);
			OutputStream os = socket.getOutputStream();
			byte[] buffer = str.getBytes();
			os.write(buffer);
		}
	}
	
	/**
	 * 弹出消息
	 * @param str
	 */
	public void alert(String str){
		Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG).show();
	}
	
	
}